# Tài liệu giáo viên

[Tailieugv.com](https://tailieugv.com/) chuyên cung cấp các bài giảng từ các giáo viên xuất sắc, các đề thi, bài ôn tập, sáng kiến kinh nghiệm được cập nhật liên tục theo sách giáo khoa mới nhất!

- Địa chỉ: Số 8 Trần Phú, P. Mộ Lao, Hà Đông, Hà Nội

- SĐT: 0814606698

Sau hơn 6 năm hoạt động, Tailieugv com đã san sẻ hơn 20.000.000 tài liệu phong phú và hữu dụng, đứng đầu các website tương tự tại Việt Nam.

có hàng nghìn bài giảng được bề ngoài từ những tri thức căn bản đến nâng cao. từ các tài liệu soạn tỉ mỉ từng tri thức theo sách giáo khoa cho đến tổng hợp tri thức và ôn tập, tập tành.

những giáo án, sáng kiến kinh nghiệm thuần tuý tới những phức tạp, chuyên sâu luôn được cập nhật mới nhất, điều chỉnh theo sự thay đổi của các Sở Giáo dục và Bộ Giáo dục. Tailieugv com đã cung ứng cho mỗi học sinh, giáo viên các chọn lựa phù thống nhất có trình độ và chỉ tiêu học tập, giảng dạy của mình.

https://www.youtube.com/channel/UCKYlNBPSS7RRFrRNJ3KizsA

https://twitter.com/tailieugv

https://www.behance.net/tailieugv/info
